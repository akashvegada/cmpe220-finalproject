from flask import Flask, render_template, request, jsonify

app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def credit_cards():
    if request.method == 'POST':
        credit_score = int(request.form['credit_score'])
        income = int(request.form['income'])
        rent = int(request.form['rent'])
        return render_template('credit_cards.html', credit_score=credit_score, income=income, rent=rent)
    return render_template('credit_cards.html')

if __name__ == '__main__':
    app.run(debug=True)